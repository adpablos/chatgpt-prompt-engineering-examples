import socket
import panel as pn
from utils import get_completion_from_messages, load_api_key


def is_port_in_use(port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        return s.connect_ex(('localhost', port)) == 0


def serve_chatbot_gui():
    if is_port_in_use(5006):
        print("A Panel server is already running. Please stop the existing server before starting a new one.")
        return

    # Initialize Panel extension
    pn.extension()

    # Initialize panel list and conversation context
    panels = []
    context = [
        {'role':'system', 'content':"""
        You are OrderBot, an automated service to collect orders for a pizza restaurant...
        """}]

    # Define your callback function
    def collect_messages(_):
        prompt = inp.value_input
        inp.value = ''
        context.append({'role':'user', 'content':f"{prompt}"})
        response = get_completion_from_messages(context)
        context.append({'role':'assistant', 'content':f"{response}"})
        panels.append(
            pn.Row('User:', pn.pane.Markdown(prompt, width=600)))
        panels.append(
            pn.Row('Assistant:', pn.pane.Markdown(response, width=600, style={'background-color': '#F6F6F6'})))

        return pn.Column(*panels)

    load_api_key()

    # Initialize your input widget and button
    inp = pn.widgets.TextInput(value="Hi", placeholder='Enter text here…')
    button_conversation = pn.widgets.Button(name="Chat!")

    # Bind your callback function to the button
    interactive_conversation = pn.bind(collect_messages, button_conversation)

    # Define your main dashboard layout
    dashboard = pn.Row(
        inp,
        button_conversation,
        pn.panel(interactive_conversation, loading_indicator=True, height=300),
    )

    # Set the servable attribute to true so that the dashboard is displayed when the script is run
    dashboard.servable()

    # Start the Panel server
    pn.serve(dashboard)


if __name__ == "__main__":
    serve_chatbot_gui()