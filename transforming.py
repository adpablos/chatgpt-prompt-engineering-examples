from utils import get_completion, load_api_key


def translate_english_to_spanish():
    load_api_key()

    prompt = """
    Translate the following English text to Spanish: 
    ```Hi, I would like to order a blender```
    """
    response = get_completion(prompt)
    print(response)


def translate_slang_to_business():
    load_api_key()

    prompt = """
    Translate the following from slang to a business letter: 
    'Dude, This is Joe, check out this spec on this standing lamp.'
    """
    response = get_completion(prompt)
    print(response)


def convert_json_to_html():
    load_api_key()

    data_json = { "resturant employees" :[
        {"name":"Shyam", "email":"shyamjaiswal@gmail.com"},
        {"name":"Bob", "email":"bob32@gmail.com"},
        {"name":"Jai", "email":"jai87@gmail.com"}
    ]}
    prompt = """
    Translate the following python dictionary from JSON to an HTML 
    table with column headers and title: {data_json}
    """
    response = get_completion(prompt)
    print(response)


def proofread_and_correct_text():
    load_api_key()

    text = [
      "The girl with the black and white puppies have a ball.",
      "Yolanda has her notebook.",
      "Its going to be a long day. Does the car need it’s oil changed?",
      "Their goes my freedom. There going to bring they’re suitcases.",
      "Your going to need you’re notebook.",
      "That medicine effects my ability to sleep. Have you heard of the butterfly affect?",
      "This phrase is to cherck chatGPT for speling abilitty"
    ]
    for t in text:
        prompt = f"""Proofread and correct the following text
        and rewrite the corrected version. If you don't find
        and errors, just say "No errors found". Don't use 
        any punctuation around the text:
        ```{t}```"""
        response = get_completion(prompt)
        print(response)


if __name__ == "__main__":
    translate_english_to_spanish()
    translate_slang_to_business()
    convert_json_to_html()
    proofread_and_correct_text()
