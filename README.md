# OpenAI GPT-3.5-turbo Examples

This repository contains Python scripts demonstrating various use cases for OpenAI's GPT-3.5-turbo model. The examples were created based on the learnings from the course [ChatGPT Prompt Engineering for Developers](https://www.deeplearning.ai/short-courses/chatgpt-prompt-engineering-for-developers/). 

The repository serves as a utility for playing with the examples and making it easier for people who want to experiment with and execute the examples. It provides a main menu for easy access to the different examples, and each script demonstrates a different usage of the GPT-3.5-turbo model.

For a detailed explanation of the experience of creating these examples and the key takeaways from the course, you can read this [blog post](https://alexdepablos.com/en/software-engineering/prompt-engineering-for-developers-chatgpt/).

The examples cover a wide range of tasks, including chat, translation, summarization, and more. Each script is standalone and can be run independently. The scripts use the OpenAI API, and you will need an API key to run them.

Each script showcases a different aspect of the model's capabilities:

- `transforming.py`: Demonstrates how to transform a technical fact sheet into a product description for a retail website.
- `chat.py`: Shows how to create a chat with the model, using a list of messages as input.
- `chatbot_gui.py`: Provides a GUI for interactive chat with the model.
- `expanding.py`: Demonstrates how to expand a short phrase into a longer, more detailed explanation.
- `inferring.py`: Shows how to infer information that isn't explicitly stated in a text.
- `iterative-prompt-development.py`: Demonstrates how to iteratively develop a prompt to get the desired output from the model.
- `prompting-guidelines.py`: Provides examples of different tactics for prompting the model.
- `summarizing.py`: Shows how to summarize a product review into a short sentence.
- `utils.py`: Contains utility functions for loading the OpenAI API key and getting completions from the model.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Python 3.7 or later
- An OpenAI API key

### Installing

1. Clone the repository:
```
git clone https://github.com/adpablos/openai-gpt-3.5-turbo-examples.git
```

2. Install the required Python packages:
```
pip install -r requirements.txt
```

3. Create a .env file in the root directory of the project, and add your OpenAI API key:
```
OPENAI_API_KEY=your-api-key
```

### Usage

This repository provides a main menu for easy access to the different examples. To run the main menu, use the Python interpreter:

```
python main.py
```

This will display a menu with the different examples available. You can select an option by entering the corresponding number. Some examples have multiple functions available, in which case a submenu will be displayed for you to choose the function you want to run.

Each Python script in the repository also demonstrates a different usage of the GPT-3.5-turbo model. You can run a script directly using the Python interpreter:

```
python script_name.py
```

Please replace script_name.py with the name of the script you want to run.

The chatbot_gui.py script is a special case because it uses the Panel library to create a web-based user interface. To run this script, you need to use the panel command followed by serve and the script name:

```
panel serve chatbot_gui.py
```

After running this command, you can access the chatbot GUI by opening a web browser and navigating to the URL http://localhost:5006/chatbot_gui.

### Contributing

We welcome and appreciate contributions to this project! If you're interested in contributing, here's a high-level overview of the steps you'll need to take:

1. **Fork the repository:** This creates your own copy of the repository under your GitHub account.

2. **Clone the forked repository:** This allows you to work on the project locally on your machine.

3. **Create a new branch:** This keeps your changes organized and separate from the main project.

4. **Make your changes:** This is where you add your contribution to the project.

5. **Commit and push your changes:** This saves your changes and uploads them to your forked repository on GitHub.

6. **Open a pull request:** This notifies us of your changes and allows us to review them before merging them into the main project.

Please ensure your code adheres to good practices and is thoroughly tested. Include comments where necessary to explain your changes. We look forward to your contributions!

