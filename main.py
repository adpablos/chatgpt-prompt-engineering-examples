import importlib
import os
import inspect
import subprocess
import warnings
from urllib3.exceptions import InsecureRequestWarning

warnings.simplefilter('ignore', category=InsecureRequestWarning)


def main():
    while True:
        print("\nWelcome to the GPT-3.5-turbo examples menu!")
        print("Please select an option:")
        print("1. chat")
        print("2. chatbot_gui")
        print("3. expanding")
        print("4. inferring")
        print("5. iterative prompt development")
        print("6. prompting guidelines")
        print("7. summarizing")
        print("8. transforming")
        print("9. Exit")

        option = input("Enter the number of your choice: ")

        if option == "9":
            print("Exiting the program. Goodbye!")
            break

        module_name = option_to_module_name(option)

        if module_name is None:
            print("Invalid option. Please try again.")
            continue

        module = importlib.import_module(module_name)

        functions = [f for f in dir(module) if callable(getattr(module, f)) and not f.startswith("__") and f not in ["load_api_key", "get_completion", "is_port_in_use"] and inspect.getmodule(getattr(module, f)) == module]

        while True:
            print(f"\nYou selected {module_name}. Please select a function to run:")
            for i, function_name in enumerate(functions, start=1):
                print(f"{i}. {function_name}")
            print(f"{len(functions) + 1}. Go back to main menu")

            function_option = input("Enter the number of your choice: ")

            if function_option == str(len(functions) + 1):
                break

            function_name = option_to_function_name(function_option, functions)

            if function_name is None:
                print("Invalid option. Please try again.")
                continue

            function = getattr(module, function_name)
            try:
                subprocess.call('cls' if os.name == 'nt' else 'clear', shell=True)  # Clear the console screen
            except Exception:
                pass  # If the command fails (e.g., because the TERM variable is not set), ignore the error
            print(f"Executing {function_name}...")
            function()


def option_to_module_name(option):
    if option == "1":
        return "chat"
    elif option == "2":
        return "chatbot_gui"
    elif option == "3":
        return "expanding"
    elif option == "4":
        return "inferring"
    elif option == "5":
        return "iterative_prompt_development"
    elif option == "6":
        return "prompting_guidelines"
    elif option == "7":
        return "summarizing"
    elif option == "8":
        return "transforming"
    else:
        return None


def option_to_function_name(option, functions):
    try:
        return functions[int(option) - 1]
    except (IndexError, ValueError):
        return None


if __name__ == "__main__":
    main()
