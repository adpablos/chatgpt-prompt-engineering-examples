from utils import load_api_key, get_completion_from_messages


def chat_shakespearean_joke():
    load_api_key()
    messages = [
        {'role': 'system', 'content': 'You are an assistant that speaks like Shakespeare.'},
        {'role': 'user', 'content': 'tell me a joke'},
        {'role': 'assistant', 'content': 'Why did the chicken cross the road'},
        {'role': 'user', 'content': "I don't know"}
    ]
    response = get_completion_from_messages(messages, temperature=1)
    print(response)


def chat_greeting():
    load_api_key()
    messages = [
        {'role': 'system', 'content': 'You are friendly chatbot.'},
        {'role': 'user', 'content': 'Hi, my name is Isa'}
    ]
    response = get_completion_from_messages(messages, temperature=1)
    print(response)


def chat_name_reminder():
    load_api_key()
    messages = [
        {'role': 'system', 'content': 'You are friendly chatbot.'},
        {'role': 'user', 'content': 'Yes, can you remind me, What is my name?'}
    ]
    response = get_completion_from_messages(messages, temperature=1)
    print(response)


def chat_name_confirmation():
    load_api_key()
    messages = [
        {'role': 'system', 'content': 'You are friendly chatbot.'},
        {'role': 'user', 'content': 'Hi, my name is Isa'},
        {'role': 'assistant', 'content': "Hi Isa! It's nice to meet you. Is there anything I can help you with today?"},
        {'role': 'user', 'content': 'Yes, you can remind me, What is my name?'}
    ]
    response = get_completion_from_messages(messages, temperature=1)
    print(response)


def chat_order_bot():
    load_api_key()
    context = [{'role': 'system', 'content': """
        You are OrderBot, an automated service to collect orders for a pizza restaurant. \
        You first greet the customer, then collects the order, \
        and then asks if it's a pickup or delivery. \
        You wait to collect the entire order, then summarize it and check for a final \
        time if the customer wants to add anything else. \
        If it's a delivery, you ask for an address. \
        Finally you collect the payment.\
        Make sure to clarify all options, extras and sizes to uniquely \
        identify the item from the menu.\
        You respond in a short, very conversational friendly style. \
        The menu includes \
        pepperoni pizza  12.95, 10.00, 7.00 \
        cheese pizza   10.95, 9.25, 6.50 \
        eggplant pizza   11.95, 9.75, 6.75 \
        fries 4.50, 3.50 \
        greek salad 7.25 \
        Toppings: \
        extra cheese 2.00, \
        mushrooms 1.50 \
        sausage 3.00 \
        canadian bacon 3.50 \
        AI sauce 1.50 \
        peppers 1.00 \
        Drinks: \
        coke 3.00, 2.00, 1.00 \
        sprite 3.00, 2.00, 1.00 \
        bottled water 5.00 \
        """}]

    # Simulate a conversation with the OrderBot
    user_messages = ["Hi", "I would like to order a pepperoni pizza", "Yes, for delivery", "My address is 123 Main St"]
    for user_message in user_messages:
        context.append({'role': 'user', 'content': user_message})
        response = get_completion_from_messages(context)
        context.append({'role': 'assistant', 'content': response})
        print(f"User: {user_message}")
        print(f"Assistant: {response}")

    # Generate a JSON summary of the order
    context.append({'role': 'system', 'content': 'create a json summary of the previous food order. Itemize the price for each item\
     The fields should be 1) pizza, include size 2) list of toppings 3) list of drinks, include size   4) list of sides include size  5)total price '})
    response = get_completion_from_messages(context, temperature=0)
    print(response)


if __name__ == "__main__":
    chat_shakespearean_joke()
    chat_greeting()
    chat_name_reminder()
    chat_name_confirmation()
    chat_order_bot()
